## A. Datenschutzerklärung

Hiermit informieren wir Sie über die Verarbeitung Ihrer personenbezogenen Daten, die beim Besuch dieser Website erhoben und verarbeitet werden sowie über die Ihnen nach den datenschutzrechtlichen Regelungen zustehenden Ansprüche und Rechte.

### Wer ist verantwortlich für den Datenschutz?

Verantwortlich für die Einhaltung der datenschutzrechtlichen Bestimmungen ist die RPL_COMPANY_NAME, RPL_COMPANY_STREET in RPL_COMPANY_ZIPCODE RPL_COMPANY_CITY, Tel: RPL_CONTACT_PHONE, [RPL_CONTACT_EMAIL](mailto:RPL_CONTACT_EMAIL), vertreten durch den Geschäftsführer, RPL_CEO_SALUTATION RPL_CEO_NAME.

### Datenschutzbeauftragter

RPL_PO_SALUTATION RPL_PO_NAME, RPL_COMPANY_NAME, RPL_COMPANY_STREET in RPL_COMPANY_ZIPCODE RPL_COMPANY_CITY. Bei Fragen zum Datenschutz können Sie sich per E-Mail unter [RPL_CONTACT_EMAIL](mailto:RPL_CONTACT_EMAIL) an ihn wenden.