### An wen geben wir die Daten weiter?

Sofern wir Daten gegenüber Auftragsverarbeitern oder Dritten offenbaren, sie an diese übermitteln oder ihnen sonst Zugriff auf die Daten gewähren, erfolgt dies nur auf Grundlage einer gesetzlichen Erlaubnis (z.B. wenn eine Übermittlung der Daten an Dritte zur Vertragserfüllung erforderlich ist, Art. 6 Abs. 1 lit. b DS-GVO), Sie eingewilligt haben (Art. 6 Abs. 1 a DS-GVO), eine rechtliche Verpflichtung dies erfordert (Art. 6 Abs. 1 c DS-GVO) oder auf Grundlage unserer berechtigten Interessen (z.B. beim Einsatz von Beauftragten, Webhostern, etc.) gemäß Art. 6 Abs. 1 f DS-GVO. Sofern wir Dritte mit der Verarbeitung von Daten auf Grundlage eines sog. „Auftragsverarbeitungsvertrages“ beauftragen, geschieht dies auf Grundlage des Art. 28 DS-GVO.

Die auf wird im Auftrag der von der RPL_COMPANY_NAME mit Sitz in RPL_COMPANY_STREET in RPL_COMPANY_ZIPCODE RPL_COMPANY_CITY durchgeführt. RPL_COMPANY_NAME ist streng zur Einhaltung der datenschutzrechtlichen Vorschriften verpflichtet. Insbesondere ist es RPL_COMPANY_NAME untersagt, Ihre personenbezogenen Daten außerhalb des Auftrags an Dritte weiterzugeben oder zu eigenen Zwecken zu verwenden. Im Folgenden finden sich die von RPL_COMPANY_NAME zur Erbringung der Leistungen herangezogenen Subunternehmer:

| Unterauftragnehmer | Verarbeitungszweck   | Serverstandort |
| ------------------ | -------------------- | -------------- |
| Muster AG          | Web-Hosting          | Deutschland    |
| Beispiel SA        | E-Mail-Versand       | Frankreich     |
| Example Ltd.       | Software-Entwicklung | Irland         |
