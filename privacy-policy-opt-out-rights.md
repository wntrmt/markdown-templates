#### Informationen über Ihr Widerspruchsrecht nach Artikel 21 DS-GVO:

Sie haben das Recht, aus Gründen, die sich aus Ihrer besonderen Situation ergeben, jederzeit gegen die Verarbeitung Sie betreffender personenbezogener Daten, die aufgrund von Art. 6 Abs. 1 e) DS-GVO (Datenverarbeitung im öffentlichen Interesse) oder Art. 6 Abs. 1 f) DS-GVO (Datenverarbeitung auf der Grundlage einer Interessenabwägung) erfolgt, Widerspruch einzulegen.

Legen Sie Widerspruch ein, werden wir Ihre personenbezogenen Daten nicht mehr verarbeiten, es sei denn, wir können zwingende berechtigte Gründe für die Verarbeitung nachweisen, die Ihre Interessen, Rechte und Freiheiten überwiegen oder die Verarbeitung dient der Geltendmachung, Ausübung oder Verteidigung von Rechtsansprüchen.

Sämtliche Rechte können Sie uns gegenüber per E-Mail über [RPL_CONTACT_EMAIL](mailto:RPL_CONTACT_EMAIL) oder über die im Punkt „Verantwortlicher“ angegebenen Kontaktdaten geltend machen.

Darüber hinaus haben Sie gemäß Art. 77 DS-GVO das Recht, sich bei Beschwerden an die zuständige Datenschutzaufsichtsbehörde zu wenden.

In der Regel können Sie sich zudem an die für Ihren üblichen Aufenthaltsort zuständige Datenschutzaufsichtsbehörde wenden.

## B. Widerrufs- und Wider­spruchs­möglich­keiten

Nachfolgend erfahren Sie, wie Sie der Verarbeitung Ihrer personenbezogenen Daten im Hinblick auf unsere Website im Einzelnen widersprechen und/oder die Löschung der hierbei erhobenen personenbezogenen Daten vornehmen können.