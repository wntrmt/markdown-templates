### Findet bei uns ein sog. Profiling statt?

Im Rahmen des Betriebs dieser Website nutzen wir keine automatisierte Entscheidungsfindung einschließlich Profiling gemäß Art. 22 DS-GVO.