### Löschen von Cookies und Do-not-track Einstellung

Sie können über Ihre Browsereinstellungen einzelne Cookies oder den gesamten Cookie-Bestand löschen. Darüber hinaus erhalten Sie Informationen und Anleitungen, wie diese Cookies gelöscht oder deren Speicherung vorab blockiert werden können, je nach Anbieter Ihres Browsers, unter den nachfolgenden Links:

[Mozilla Firefox](https://support.mozilla.org/de/kb/cookies-loeschen-daten-von-websites-entfernen)

[Internet Explorer](https://support.microsoft.com/de-de/help/17442/windows-internet-explorer-delete-manage-cookies)

[Google Chrome](https://support.google.com/accounts/answer/61416?hl=de)

[Opera](http://www.opera.com/de/help)

[Safari](https://support.apple.com/de-de/guide/safari/sfri11471/12.0/mac)

Sie können auch die Cookies vieler Unternehmen und Funktionen einzeln verwalten, die für Werbung eingesetzt werden. Verwenden Sie dazu die entsprechenden Nutzertools, abrufbar unter [aboutads.info choices](https://www.aboutads.info/choices/) oder [Your Online Choices](http://www.youronlinechoices.com/uk/your-ad-choices).

Die meisten Browser bieten zudem eine sog. „Do-Not-Track-Funktion“ an, mit der Sie angeben können, dass Sie nicht von Websites „verfolgt“ werden möchten. Wenn diese Funktion aktiviert ist, teilt der jeweilige Browser Werbenetzwerken, Websites und Anwendungen mit, dass Sie nicht zwecks verhaltensbasierter Werbung und Ähnlichem verfolgt werden möchten. Informationen und Anleitungen, wie Sie diese Funktion bearbeiten können, erhalten Sie je nach Anbieter Ihres Browsers, unter den nachfolgenden Links:

[Mozilla Firefox](https://www.mozilla.org/de/firefox/dnt/)

[Internet Explorer](https://support.microsoft.com/de-de/help/17288/windows-internet-explorer-11-use-do-not-track)

[Google Chrome](https://support.google.com/chrome/answer/2790761?co=GENIE.Platform%3DDesktop&hl=de)

[Opera](http://help.opera.com/Windows/12.10/de/notrack.html)

[Safari](https://support.apple.com/de-de/guide/safari/prevent-websites-from-tracking-you-sfri40732/mac)