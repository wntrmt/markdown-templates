#### Squarespace Analytics

Diese Website wird von Squarespace gehostet. Squarespace sammelt personenbezogene Daten, wenn Sie diese Website besuchen. Dazu gehören:

* Informationen über Ihren Browser, Ihr Netzwerk und Ihr Gerät
* Webseiten, die Sie vor dem Besuch dieser Website aufgerufen haben
* Ihre IP-Adresse

Squarespace benötigt die Daten für den Betrieb dieser Website sowie zum Schutz und zur Verbesserung seiner Plattform und Services. Squarespace analysiert die Daten in entpersonalisierter Form.

Darüber hinaus sammelt diese Website personenbezogene Daten, die als Grundlage für unsere Website-Analytics dienen. Dazu gehören:

* Informationen über Ihren Browser, Ihr Netzwerk und Ihr Gerät
* Webseiten, die Sie vor dem Besuch dieser Website aufgerufen haben
* Ihre IP-Adresse

Diese Informationen können auch Details über Ihre Nutzung dieser Website enthalten, einschließlich:

* Klicks
* Interne Links
* Besuchte Seiten
* Scrollen
* Suchvorgänge
* Zeitstempel

Wir teilen diese Informationen mit Squarespace, unserem Anbieter für Website-Analytics, um mehr über den Traffic und die Aktivität auf dieser Website zu erfahren.

Neben der oben beschriebenen Datenerfassung, werden beim Besuch unserer Website die folgenden Cookies durch Squarespace gesetzt:

* [Diese funktionellen und erforderlichen Cookies werden immer verwendet](https://support.squarespace.com/hc/articles/360001264507#toc-functional-and-required-cookies), da sie es Squarespace, unserer Hosting-Plattform, ermöglichen, diese Website sicher für Sie bereitzustellen.
* [Diese Analytics- und Performance-Cookies](https://support.squarespace.com/hc/articles/360001264507#toc-analytics-and-performance-cookies) werden nur dann wie im Folgenden beschrieben auf dieser Website verwendet, wenn Sie unser Cookie-Banner bestätigen. Wir verwenden Analytics-Cookies, um Einblick in den Website-Traffic, die Website-Aktivität und weitere Daten zu erhalten.
