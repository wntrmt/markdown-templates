#### Google Universal Analytics mit IP-Anonymisierung

Wir nutzen auf unserer Website Google Analytics, einen Webanalysedienst der Google LLC, 1600 Amphitheatre Parkway, Mountain View, CA 94043, USA (im Folgenden: „Google“). In diesem Zusammenhang werden pseudonyme Nutzungsprofile erstellt und [Cookies](#cookies) verwendet.

Die Verwendung von Google Analytics erfolgt im Rahmen unserer berechtigten Interessen (Art. 6 Abs. 1 Satz 1 lit. f DSGVO) an der Analyse und Optimierung unseres Onlineangebotes sowie dem wirtschaftlichen Betrieb dieser Website. Daher verarbeitet Google die Informationen in unserem Auftrag, um die Nutzung der Website auszuwerten, um Reports über die Website-Aktivitäten zusammenzustellen und um weitere mit der Website-Nutzung und der Internetnutzung verbundene Dienstleistungen zu Zwecken der Marktforschung und bedarfsgerechten Gestaltung dieser Internetseiten gegenüber uns zu erbringen.

Wir setzen Google Analytics nur mit aktivierter IP-Anonymisierung ein. Das bedeutet, die IP-Adresse der Nutzer wird von Google innerhalb von Mitgliedstaaten der Europäischen Union oder in anderen Vertragsstaaten des Abkommens über den Europäischen Wirtschaftsraum gekürzt. Nur in Ausnahmefällen wird die volle IP-Adresse an einen Server von Google in den USA übertragen und dort gekürzt. Die IP-Adresse wird nicht mit anderen Daten von Google zusammengeführt.

Hinsichtlich des Einsatzes von Google Analytics werden die durch das entsprechende Cookie erzeugten Informationen über Benutzung des Onlineangebotes in der Regel an einen Server von Google in den USA übertragen und dort gespeichert. Gegebenenfalls werden die erfassten Daten an Dritte übertragen, sofern dies gesetzlich vorgeschrieben ist oder soweit Dritte die Daten im Auftrag verarbeiten.

##### Widerspruch (Opt-Out)

Sie können die Installation der Cookies durch eine entsprechende Einstellung der Browser-Software verhindern. Wir weisen jedoch darauf hin, dass in diesem Fall gegebenenfalls nicht sämtliche Funktionen dieser Website vollumfänglich genutzt werden können. Sie können darüber hinaus die Erfassung der durch das Cookie erzeugten und auf Ihre Nutzung der Website bezogenen Daten (inkl. Ihrer IP-Adresse) sowie die Verarbeitung dieser Daten durch Google verhindern, indem Sie ein [Browser-Add-on herunterladen und installieren](https://tools.google.com/dlpage/gaoptout?hl=de).

Es wird ein Opt-Out-Cookie gesetzt, das die zukünftige Erfassung Ihrer Daten beim Besuch dieser Website verhindert. Das Opt-Out-Cookie gilt nur in diesem Browser und nur für unsere Website und wird auf Ihrem Gerät abgelegt. Löschen Sie die Cookies in diesem Browser, müssen Sie das Opt-Out-Cookie erneut setzen.

Weitere Informationen zum Datenschutz im Zusammenhang mit Google Analytics finden Sie etwa in der [Google Analytics-Hilfe](https://support.google.com/analytics/answer/6004245). Informationen zur Datennutzung von Google finden Sie in deren [Datenschutzerklärung.](https://policies.google.com/privacy)