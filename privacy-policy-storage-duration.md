### Wie lange speichern wir Ihre Daten?

a) IP-Adresse, Datum und Uhrzeit des Aufrufs, Art des Aufrufs, Serverstatus, übertragene Bytes, Referrer und User Agent speichern wir längstens bis zwei Jahre nach Abschluss der Publikumsabstimmung. Einzelheiten zu den Cookies, deren Speicherdauer und wie Sie diese Daten löschen können, finden Sie unter B. Ziff. 1.

b) Die von Google gespeicherte anonymisierte IP-Adresse wird nach 14 Monaten automatisch gelöscht. Einzelheiten zu den Cookies, deren Speicherdauer und wie Sie diese Daten löschen können, finden Sie unter B. Ziff. 1.