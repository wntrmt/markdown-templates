#### Google Tag Manager

Auf unserer Website kommt das Tool Google Tag Manager der Google LLC, 1600 Amphitheatre Parkway, Mountain View, CA 94043, USA (im Folgenden: „Google“) zum Einsatz. Mit dem Google Tag Manager verwalten wir die Tools, über die wir in dieser Datenschutzerklärung informieren. Einzelheiten bezüglich dieser Tools entnehmen Sie daher der Information bezüglich des konkreten Tools.

Das Tool Tag Manager selbst (das die Tags implementiert) ist eine cookielose Domain. Das Tool sorgt für die Auslösung anderer Tags, die ihrerseits unter Umständen Daten erfassen. Google Tag Manager greift nicht auf diese Daten zu. Wenn auf Domain- oder Cookie-Ebene eine Deaktivierung vorgenommen wurde, bleibt diese für alle Tracking-Tags bestehen, die mit Google Tag Manager implementiert werden.

Weitere Informationen zum Google Tag Manager finden Sie in den Nutzungsrichtlinien für dieses [Produkt](https://marketingplatform.google.com/intl/de/about/analytics/tag-manager/use-policy/).