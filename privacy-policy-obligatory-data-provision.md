### Besteht eine Pflicht zur Bereitstellung der personenbezogenen Daten?

Es besteht keine gesetzliche Pflicht zur Bereitstellung. Ohne die Angabe der Daten ist die Nutzung einer Funktionen nur eingeschränkt oder gar nicht möglich (z.B. Kontakt via Kontakformular).