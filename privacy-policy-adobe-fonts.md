#### Adobe Fonts

Adobe Fonts ist ein Dienst, der den Zugriff auf eine Schriftenbibliothek zur Verwendung in Websites ermöglicht.

##### Wie verwendet der Dienst „Adobe Fonts“ für Websites Cookies?

Bei der Bereitstellung der Adobe Fonts-Website ([fonts.adobe.com](https://fonts.adobe.com/?locale=de)) verwenden wir [Cookies](https://www.adobe.com/de/privacy/cookies.html) gemäß unserer Datenschutzerklärung.

Im Zuge der Bereitstellung des Diensts „Adobe Fonts“ für Websites platzieren oder verwenden wir keine Cookies auf Websites, um unsere Schriften bereitzustellen.

##### Welche Informationen werden vom Dienst „Adobe Fonts“ für Websites erfasst?

Zur Bereitstellung des Diensts „Adobe Fonts“ für Websites kann Adobe Informationen über die Schriften bzw. Schriftarten erfassen, die für Ihre Website bereitgestellt werden. Die Informationen werden zur Abrechnung und Einhaltung von Vorschriften verwendet und können Folgendes umfassen:

*   bereitgestellte Schriften
*   ID des Webprojekts
*   JavaScript-Version des Webprojekts (String)
*   Art des Webprojekts (String „configurable“ oder „dynamic“)
*   Einbettungstyp (ob Sie den JavaScript- oder CSS-Einbettungscode verwenden)
*   Konto-ID (identifiziert den Kunden, von dem das Webprojekt stammt)
*   Dienst, der die Schriftarten bereitstellt (z. B. Adobe Fonts)
*   Server, der die Schriftarten bereitstellt (z. B. Server von Adobe Fonts oder Unternehmens-CDN)
*   Hostname der Seite, auf der die Schriften geladen werden
*   Die Zeit, die der Webbrowser zum Herunterladen der Schriften benötigt
*   Die Zeit vom Herunterladen der Schriften mit dem Webbrowser bis zur Anwendung der Schriften
*   Ob ein Werbeblocker installiert ist, um festzustellen, ob der Werbeblocker die korrekte Verfolgung der Seitenaufrufe beeinträchtigt
*   Betriebssystem- und Browser-Version

##### Wie verwendet der Dienst „Adobe Fonts“ für Websites die von ihm erfassten Informationen?

Adobe verwendet die von Websites, die Adobe Fonts nutzen, erfassten Informationen, um den Dienst „Adobe Fonts“ bereitzustellen und Liefer- oder Download-Probleme zu diagnostizieren. Diese Informationen werden auch genutzt, um die Verträge von Adobe mit den Schriftenherstellern, deren Schriften verwendet werden, zu bezahlen und zu erfüllen. Wir teilen aggregierte Berichte mit Schriftenherstellern und bestätigen möglicherweise einem Schriftenhersteller gegenüber, dass Sie über eine gültige Lizenz von Adobe verfügen, geben aber anderweitig Ihre personenbezogenen Informationen nicht an Schriftenhersteller weiter.
